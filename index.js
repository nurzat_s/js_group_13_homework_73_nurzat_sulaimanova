const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('Main\n');
});

app.get('/:name', (req, res) => {
    res.send(`${req.params.name}\n`);
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});