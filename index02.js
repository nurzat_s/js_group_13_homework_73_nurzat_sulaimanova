const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;

const password = 'password';

app.get('/:anyword', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(`${req.params.anyword}\n`));
});

app.get('/home/:hello', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(`${req.params.hello}\n`));
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});

